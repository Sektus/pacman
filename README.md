## Pacman 

Wybrałem jako projekt zaliczeniowy z modułu `C++` zaprogramowanie mojej własnej implementacji gry `Pacman` w tym języku. W swojej implementacji korzystałem z biblioteki `SDL`

W `Pacman` gracz kieruje żółtą kulką – tytułowym `Pacmanem` poprzez labirynt pełen białych kulek. Warunkiem przejścia do następnego poziomu jest zjedzenie ich wszystkich. Ponadto należy unikać czterech duszków (różowego – `Pinky`, błękitnego – `Inky`, czerwonego – `Blinky` i pomarańczowego – `Clyde`), które poruszają się po całym labiryncie. Gra się kończy gdy wszystkie życia Pacman'a skończyły się, a celem gry jest uzyskanie największej ilości oczek.

Więcej o mechanice gry (moja implementacji nie zawiera ich wszystkich):
[Pacman](https://www.gamasutra.com/view/feature/3938/the_pacman_dossier.php?print=1)

W meni gry można przesuwać się między opcjami za pomocą śtriałek. Przy wybiorze opcji `start` zaczyna się nowa gra. Żeby kierować się pacman'em trzeba używać liter `W`, `A`, `S`, `D`. 

## Usage 

```bash
make #budowanie projektu
```

```bash
./pacman.out #uruchomienie gry
```