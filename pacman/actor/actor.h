#ifndef PACMAN_ACTOR_H
#define PACMAN_ACTOR_H

#include <SDL2/SDL.h>
class Point{
  public:
    int x, y;
    Point();
    Point(int a, int b);
};

enum Direction{
  LEFT = 0,
  RIGHT = 4,
  UP = 1,
  DOWN = 5,
  NONE = 9
};

bool isSameAxis(const Direction f, const Direction s);
bool isOppositeDirection(const Direction f, const Direction s);

class Tile{
  public:
    int x, y;
    Tile();
    Tile(Point pos);
    Tile(int xx, int yy);
    Tile next(const Direction dir);
    Point getCenter() const;
    float distance(const Tile &cls) const;
    friend bool operator==(const Tile &lhs, const Tile &rhs);
};

class Actor{
  protected:
    Direction dir = NONE;
    Direction nextDir = NONE;
    Point position;
    Point real;
    int16_t speed = 0;
    int8_t steps;   //steps to next tile centre
    Actor();

  public:
    virtual void draw();
    virtual void reset();
    virtual void nextPosition();
    void setSpeed(int16_t speed);
    Tile getTile() const;
    Point getPosition() const;
    Direction getDirection() const;
    Point move();
    bool collided(const Actor *other) const;
};

#endif