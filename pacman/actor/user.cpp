#include <pacman/actor/user.h>

User::User(Maze *context): Actor(){
  this->context = context;
  reset();
}

void User::nextDirection(Direction d){
  Point next = curr;
  if (d == RIGHT){
    next.x += BLOCK * 100;
    dx = speed;
    dy = 0;
    steps = (next.x - position.x) / dx;
  } else if (d == LEFT){
    next.x -= BLOCK * 100;
    dx = -speed;
    dy = 0;
    steps = (next.x - position.x) / dx;
  } else if (d == UP){
    next.y -= BLOCK * 100;
    dx = 0;
    dy = -speed;
    steps = (next.y - position.y) / dy;
  } else {
    next.y += BLOCK * 100;
    dx = 0;
    dy = speed;
    steps = (next.y - position.y) / dy;
  }

  bool floor = context->isFloor(next.x / (BLOCK * 100), next.y / (BLOCK * 100));
  if (floor){
    dir = d;
    curr = next;
  } else {
    dy = 0;
    dx = 0;
    steps = -1;
    dir = NONE;
    nextDir = NONE;
  }
}

void User::nextPosition(){
  /* handle direction changes by user input */
  handleNewDirection(); 

  /* user doesnt move */
  if (dir == NONE) {return;}

  /* handle tunnel */
  if (real.x < - BLOCK / 2){
    real.x = context->map_width * BLOCK + BLOCK / 2;
    position.x = real.x * 100;
    curr.x = (real.x) * 100;
    steps = 0;
  } else if (real.x > context->map_width * BLOCK + BLOCK / 2){
    real.x = - BLOCK / 2;
    position.x = real.x * 100;
    curr.x = (real.x) * 100;
    steps = 0;
  }

  if (corner == 0){
    if (dir == RIGHT || dir == LEFT){
      dy = 0;
      position.y = curr.y;
    } else {
      dx = 0;
      position.x = curr.x; 
    }
  }

  if (steps == 0){
    int diff;
    Direction prevDir = dir;
    Point prevTarget = curr;
    corner = -1;

    nextDirection(dir);
    if (prevDir == RIGHT || prevDir == LEFT){
      diff = prevTarget.x - position.x;
      position.x = prevTarget.x;
    } else {
      diff = prevTarget.y - position.y;
      position.y = prevTarget.y;
    }
    
    if (dir == RIGHT || dir == LEFT){
      position.x += dx - diff;
    } else if (dir != NONE){
      position.y += dy - diff;
    }
  } else {
    position.x += dx;
    position.y += dy;
  }
  
  real.x = position.x / 100;
  real.y = position.y / 100;
  --steps;
  --corner;
}

void User::handleNewDirection(){
  if (dir != nextDir){
    Point next = curr;
    switch (dir){
      case RIGHT:
        if (nextDir == LEFT){
          //opposite direction
          dx = -speed;
          next.x -= BLOCK * 100;
          steps = (next.x - position.x) / dx;
          curr = next;
          dir = nextDir;
        } else {
          //cornering
          next.x = (real.x / BLOCK * BLOCK + BLOCK / 2) * 100;
          if (nextDir == UP){
            next.y -= BLOCK * 100;
            dy = -speed;
          } else {
            next.y += BLOCK * 100;
            dy = speed;
          }

          bool floor = context->isFloor(next.x / (BLOCK * 100), next.y / (BLOCK * 100));
          if (floor){
            corner = (next.x - position.x) / dx;
            if (corner < 0){
              dx = -dx;
              corner = -corner;
            } 
            steps = (next.y - position.y) / dy;
            curr = next;
            dir = nextDir;
          } else {
            dy = 0;
          }
        }
        break;
      case LEFT:
        if (nextDir == RIGHT){
          dx = speed;
          next.x += BLOCK * 100;
          steps = (next.x - position.x) / dx;
          curr = next;
          dir = nextDir;
        } else {
          //cornering
          next.x = (real.x / BLOCK * BLOCK + BLOCK / 2) * 100;
          if (nextDir == UP){
            next.y -= BLOCK * 100;
            dy = -speed;
          } else {
            next.y += BLOCK * 100;
            dy = speed;
          }

          bool floor = context->isFloor(next.x / (BLOCK * 100), next.y / (BLOCK * 100));
          if (floor){
            corner = (next.x - position.x) / dx;
            if (corner < 0){
              dx = -dx;
              corner = -corner;
            } 
            steps = (next.y - position.y) / dy;
            curr = next;
            dir = nextDir;
          } else {
            dy = 0;
          }
        }
        break;
      case UP:
        if (nextDir == DOWN){
          dy = speed;
          next.y += BLOCK * 100;
          steps = (next.y - position.y) / dy;
          curr = next;
          dir = nextDir;
        } else {
          //cornering
          next.y = (real.y / BLOCK * BLOCK + BLOCK / 2) * 100;
          if (nextDir == LEFT){
            next.x -= BLOCK * 100;
            dx = -speed;
          } else {
            next.x += BLOCK * 100;
            dx = speed;
          }

          bool floor = context->isFloor(next.x / (BLOCK * 100), next.y / (BLOCK * 100));
          if (floor){
            corner = (next.y - position.y) / dy;
            if (corner < 0){
              dy = -dy;
              corner = -corner;
            } 
            steps = (next.x - position.x) / dx;
            curr = next;
            dir = nextDir;
          } else {
            dx = 0;
          }
        }
        break;
      case DOWN:
        if (nextDir == UP){
          dy = -speed;
          next.y -= BLOCK * 100;
          steps = (next.y - position.y) / dy;
          curr = next;
          dir = nextDir;
        } else {
          //cornering
          next.y = (real.y / BLOCK * BLOCK + BLOCK / 2) * 100;
          if (nextDir == LEFT){
            next.x -= BLOCK * 100;
            dx = -speed;
          } else {
            next.x += BLOCK * 100;
            dx = speed;
          }

          bool floor = context->isFloor(next.x / (BLOCK * 100), next.y / (BLOCK * 100));
          if (floor){
            corner = (next.y - position.y) / dy;
            if (corner < 0){
              dy = -dy;
              corner = -corner;
            } 
            steps = (next.x - position.x) / dx;
            curr = next;
            dir = nextDir;
          } else {
            dx = 0;
          }
        }
        break;
      case NONE:
        nextDirection(nextDir);
        break;
    }
  }
}

void User::reset(){
  dir = NONE;
  nextDir = NONE;

  real.x = 14 * BLOCK;
  position.x = real.x * 100;
  curr.x = (13 * BLOCK + BLOCK / 2) * 100;

  real.y = 26 * BLOCK + BLOCK / 2;
  position.y = real.y * 100;
  curr.y = position.y;

  steps = -1;
  corner = -1;
}

void User::draw(){
  SDL_Rect dst = {real.x - BLOCK, real.y - BLOCK, BLOCK * 2, BLOCK * 2};
  SDL_Rect src = {0, 72, BLOCK * 2, BLOCK * 2};

  long tick = context->getTicks();
  if (dir == NONE || tick % 5 == 2){
    src.y = 168;
  } else {
    src.x = dir * BLOCK * 2;
    if (tick % 5 == 1 || tick % 5 == 3){
      src.x += 48;
    }
  }
  context->draw(&src, &dst);
}

void User::drawDead(int t){
  SDL_Rect dst = {real.x - BLOCK, real.y - BLOCK, BLOCK * 2, BLOCK * 2};
  SDL_Rect src = {0, 168, BLOCK * 2, BLOCK * 2};
  if (t > 0){
    src.x = 96 + t * 24;
  }
  context->draw(&src, &dst);
}

void User::handleKey(const SDL_Keycode key){
  switch (key){
    case SDLK_a:
      nextDir = LEFT;
      break;
    case SDLK_d:
      nextDir = RIGHT;
      break;
    case SDLK_w:
      nextDir = UP;
      break;
    case SDLK_s:
      nextDir = DOWN;
      break;
  }
}