#include <pacman/actor/ghost.h>

const Direction Ghost::dirs[4] = { UP, LEFT, DOWN, RIGHT};

Ghost::Ghost(Maze *ctx, SDL_Rect spr){
  context = ctx;
  this->sprite = spr;
}

Tile Ghost::getTargetTile() const{}

void Ghost::draw(){
  SDL_Rect dst = {real.x - BLOCK, real.y - BLOCK, BLOCK * 2, BLOCK * 2};
  SDL_Rect src = {sprite.x, sprite.y, BLOCK * 2, BLOCK * 2};

  long tick = context->getTicks();
  handleState();
  if (state == FRIGHTENED){
    int32_t diff = tick -  eatable;
    if (eatable < 5 * FPS && eatable % FPS / 2 < FPS / 4){
      src.x = 144;
      src.y = 96;
    } else {
      src.x = 192;
      src.y = 96;
    }
  } else {
    if (state == EATEN){
      src.x = 192;
      src.y = 216;
    }
    switch (dir){
      case LEFT:
        src.x += 96;
        break;
      case DOWN:
        src.x += 48;
        break;
      case UP:
        src.x += 144;
        break;
    }
  }
  if (tick % BLOCK >= BLOCK / 2){
      src.x += 24;
  }
  context->draw(&src, &dst);
}

void Ghost::reset(){
  eatable = -1;
  eaten = -1;
  chase = -1;

  real.x = 14 * BLOCK;
  position.x = real.x * 100;
  curr.x = 13;

  real.y = 14 * BLOCK + BLOCK / 2;
  position.y = real.y * 100;
  curr.y = 14;

  dir = LEFT;
  nextDir = LEFT;

  state = SCATTER;
  scatter = 7 * FPS;

  steps = (BLOCK / 2) * 100 / speed;
  nextDirection();
}

bool Ghost::isVunerable(){
  return eatable != -1;
}

bool Ghost::isDangerous(){
  return eaten == -1;
}

bool Ghost::isHidden(){
  return eatable == -1 && eaten != -1;
}

void Ghost::eat(){
  eatable = -1;
  eaten = 4 * FPS;
  state = EATEN;
}

void Ghost::makeEatable(){
  stored = state;
  state = FRIGHTENED;
  switch (dir){
    case LEFT:
      dir = RIGHT;
      curr.x += 1;
      steps = (curr.x * BLOCK * 100 + BLOCK * 50 - position.x) / speed;
      break;
    case RIGHT:
      dir = LEFT;
      curr.x -= 1;
      steps = (position.x - curr.x * BLOCK * 100 - BLOCK * 50) / speed;
      break;
    case UP:
      dir = DOWN;
      curr.y += 1;
      steps = (curr.y * BLOCK * 100 + BLOCK * 50 - position.y) / speed;
      break;
    case DOWN:
      dir = UP;
      curr.y -= 1;
      steps = (position.y - curr.y * BLOCK * 100 - BLOCK * 50) / speed;
      break;
  }
  
  eatable = 8 * FPS;
  randomDirection();
}


void Ghost::nextPosition() {
  if (steps == 0){
    Point centre(curr.x * BLOCK * 100 + BLOCK * 50, curr.y * BLOCK * 100 + BLOCK * 50);
    int diff;

    if (dir == LEFT || dir == RIGHT){
      diff = centre.x - position.x;
      position.x = centre.x;
      real.x = position.x / 100;
    } else {
      diff = centre.y - position.y;
      position.y = centre.y;
      real.y = position.y / 100;
    }
    if (diff < 0){
      diff = - diff;
    }
    steps = (BLOCK * 100 + diff) / speed;

    /* handle tunnel */
    if (real.x < - BLOCK / 2){
      real.x = context->map_width * BLOCK + BLOCK / 2;
      position.x = real.x * 100;
      curr.x = context->map_width;
      nextDir = LEFT;
    } else if (real.x > context->map_width * BLOCK + BLOCK / 2){
      real.x = - BLOCK / 2;
      position.x = real.x * 100;
      curr.x = -1;
      nextDir = RIGHT;
    }

    dir = nextDir;

    switch (dir){
      case LEFT:
        position.x -= speed - diff;
        real.x = position.x / 100;
        --curr.x;
        break;
      case RIGHT:
        position.x += speed - diff;
        real.x = position.x / 100;
        ++curr.x;
        break;
      case UP:
        position.y -= speed - diff;
        real.y = position.y / 100; 
        --curr.y;
        break;
      case DOWN:
        position.y += speed - diff;
        real.y = position.y / 100; 
        ++curr.y;
        break;
    }
    nextDirection();
    Point centre2(curr.x * BLOCK * 100 + BLOCK * 50, curr.y * BLOCK * 100 + BLOCK * 50);
  } else {
    switch (dir){
      case LEFT:
        position.x -= speed;
        real.x = position.x / 100;
        break;
      case RIGHT:
        position.x += speed;
        real.x = position.x / 100;
        break;
      case UP:
        position.y -= speed;
        real.y = position.y / 100; 
        break;
      case DOWN:
        position.y += speed;
        real.y = position.y / 100; 
        break;
    }
  }
  --steps;
}

void Ghost::nextDirection(){
  if (state == FRIGHTENED){
    randomDirection();
  } else {
    if (state == EATEN){
      target.x = 14;
      target.y = 14;
    } else {
      target = getTargetTile();
    }

    Tile next;
    float minDist = std::sqrt(context->map_width * context->map_width + context->map_height * context->map_height);
    float dist;

    for (char i = 0; i < 4; ++i){
      if (!isOppositeDirection(dir, dirs[i])){
        next = curr.next(dirs[i]);
        dist = target.distance(next);
        if (dist < minDist && !context->isWall(next.x, next.y)){
          minDist = dist;
          nextDir = dirs[i];
        }
      }
    }
  }
}

void Ghost::randomDirection(){
  int r;
  do {
    r = std::rand() % 4;
  } while (isOppositeDirection(dir, dirs[r]));
  
  Tile next = curr.next(dirs[r]);
  if (!context->isWall(next.x, next.y)){
    nextDir = dirs[r];
  } else {
    for (int i = 0; i < 4; ++i){
      next = curr.next(dirs[i]);
      if (!context->isWall(next.x, next.y) && !isOppositeDirection(dir, dirs[i])){
        nextDir = dirs[i];
      }
    }
  }
}

void Ghost::handleState() {
  if (state == FRIGHTENED && --eatable <= -1){
    state = stored;
  } else if (state == EATEN && --eaten <= -1){
    state = stored;
  } 

  if (state == SCATTER && --scatter <= -1){
    state = CHASE;
    chase = 20 * FPS;
  } else if (state == CHASE && --chase <= -1){
    state = SCATTER;
    scatter = 7 * FPS;
  }
}
