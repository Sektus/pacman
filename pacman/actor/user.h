#ifndef PACMAN_USER_H
#define PACMAN_USER_H

#include <pacman/actor/actor.h>
class Actor;

#include <pacman/screen/maze.h>
class Maze;

class User: public Actor{
  private:
    Maze *context;
    Point curr;
    int32_t dx;      //horizontal speed
    int32_t dy;      //vertical speed
    int32_t corner;  //steps for cornering
    void nextDirection(const Direction d);
    void nextPosition();
    void handleNewDirection();

  public:
    User(Maze *context);
    void reset();
    void draw();
    void drawDead(const int32_t t);
    void handleKey(const SDL_Keycode key); 
};

#endif