#ifndef PACMAN_GHOST_H
#define PACMAN_GHOST_H

#include <pacman/actor/actor.h>
class Actor;

#include <pacman/screen/maze.h>
class Maze;

class Ghost: public Actor{
  protected:
    enum State {
      CHASE = 0,
      SCATTER = 1,
      FRIGHTENED = 2,
      EATEN = 3
    };
    Maze *context = nullptr;
    State state;
    virtual Tile getTargetTile() const;
    
  public:
    Ghost(Maze *ctx, SDL_Rect spr);
    void draw();
    void reset();
    bool isVunerable();
    bool isDangerous();
    bool isHidden();
    void eat();
    void makeEatable();
  
  private:
    static const Direction dirs[4];
    int32_t chase;
    int32_t scatter;
    int32_t eatable;
    int32_t eaten;
    SDL_Rect sprite;
    State stored;
    Tile curr;
    Tile target;
    void nextPosition();
    void nextDirection();
    void randomDirection();
    void handleState();
};

#endif 