#include <pacman/actor/ghosts.h>

static const SDL_Rect clydeSprite = {0, 216, BLOCK * 2, BLOCK * 2};
static const SDL_Rect blinkySprite = {0, 144, BLOCK * 2, BLOCK * 2};
static const SDL_Rect pinkySprite = {0, 192, BLOCK * 2, BLOCK * 2};
static const SDL_Rect inkySprite = {192, 192, BLOCK * 2, BLOCK * 2};

Clyde::Clyde(Maze *context): Ghost(context, clydeSprite){}

Tile Clyde::getTargetTile() const {
  Tile target(0, context->map_height);
  if (state == CHASE){
    Tile user = context->getUserTile();
    Tile self = Tile(real);
    if (user.distance(self) > 8){
      return user;
    }
  }
  return target;
}

Blinky::Blinky(Maze *context): Ghost(context, blinkySprite){}

Tile Blinky::getTargetTile() const {
  Tile target(0, 0);
  if (state == CHASE){
    Tile user = context->getUserTile();
    return user;
  }
  return target;
}

Inky::Inky(Maze *context): Ghost(context, inkySprite){}

Tile Inky::getTargetTile() const {
  Tile target(context->map_width, context->map_height);
  if (state == CHASE){
    Tile user = context->getUserTile();
    Direction d = context->getUserDirection();
    for (int i = 0; i < 2; ++i){
      user = user.next(d).next(LEFT);
    }
    return user;
  }
  return target;
}

Pinky::Pinky(Maze *context): Ghost(context, pinkySprite){}

Tile Pinky::getTargetTile() const {
  Tile target(context->map_width, 0);
  if (state == CHASE){
    Tile user = context->getUserTile();
    Direction d = context->getUserDirection();
    for (int i = 0; i < 4; ++i){
      user = user.next(d);
    }
    return user;
  }
  return target;
}