#ifndef PACMAN_GHOSTS_H
#define PACMAN_GHOSTS_H

#include <pacman/actor/ghost.h>

class Clyde: public Ghost {
  private:
    virtual Tile getTargetTile() const;
  
  public:
    Clyde(Maze *context);
};

class Blinky: public Ghost{
  private:
    virtual Tile getTargetTile() const;
  
  public:
    Blinky(Maze *context);
};

class Inky: public Ghost{
  private:
    virtual Tile getTargetTile() const;
  
  public:
    Inky(Maze *context);
};

class Pinky: public Ghost{
  private:
    virtual Tile getTargetTile() const;
  
  public:
    Pinky(Maze *context);
};



#endif