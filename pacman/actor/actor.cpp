#include <pacman/actor/actor.h>
#include <pacman/game/game.h>

Point::Point(): x(0), y(0){}
Point::Point(int a, int b): x(a), y(b){}

bool isSameAxis(const Direction f, const Direction s){
  if (((f == LEFT || f == RIGHT) &&
         (s == LEFT || s == RIGHT)) ||
        ((f == UP || f == DOWN) &&
         (s == UP || s == DOWN)) || f == NONE)
  {
    return true;
  } else {
    return false;
  }
}

bool isOppositeDirection(const Direction f,const Direction s){
  if ((f == LEFT && s == RIGHT) ||
      (f == RIGHT && s == LEFT) ||
      (f == UP && s == DOWN) ||
      (f == DOWN && s == UP))
  {
    return true;
  } else {
    return false;
  }
}

Tile::Tile():x(0), y(0){}

Tile::Tile(Point p){
  x = p.x / BLOCK;
  y = p.y / BLOCK;
}

Tile::Tile(int xx, int yy): x(xx), y(yy){}

Tile Tile::next(const Direction dir){
  int nx = x, ny = y;
  switch (dir){ 
    case Direction::DOWN:
      ++ny;
      break;
    case Direction::UP:
      --ny;
      break;
    case Direction::LEFT:
      --nx;
      break;
    case Direction::RIGHT:
      ++nx;
      break; 
  }

  return Tile(nx, ny);
}

Point Tile::getCenter() const{
  return Point(x * 100 * BLOCK + 100 * BLOCK / 2, y * 100 * BLOCK + 100 * BLOCK / 2);
}

float Tile::distance(const Tile &cls) const{
  int x = this->x - cls.x;
  int y = this->y - cls.y;
  return std::sqrt(x*x + y*y);
}

bool operator==(const Tile &lhs, const Tile &rhs){
  return lhs.x == rhs.x && lhs.y == rhs.y;
}

Actor::Actor(): position(0, 0){}

void Actor::draw(){}
void Actor::reset(){}
void Actor::nextPosition(){}

void Actor::setSpeed(int16_t speed){
  this->speed = speed;
}

Tile Actor::getTile() const {
  return Tile(real);
}

Point Actor::getPosition() const {
  return real;
}

Direction Actor::getDirection() const {
  return dir;
}

Point Actor::move(){
  Point prev = real;
  nextPosition();
  return prev;
}

bool Actor::collided(const Actor *other) const {
  return getTile() == other->getTile();
}