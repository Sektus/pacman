#include <pacman/input/input.h>

Input::Input(){}

bool Input::update(){
  return SDL_PollEvent(&event);
}

bool Input::isExit() const{
  return event.type == SDL_QUIT;
}

bool Input::isWindowEvent() const{
  return event.type == SDL_WINDOWEVENT;
}

uint8_t Input::getWindowEvent() const{
  return event.window.event;
}

SDL_Keycode Input::getButtonPressed() const{
  return  event.key.keysym.sym;
}

bool Input::isButtonPressed() const{
  return event.type == SDL_KEYDOWN;
}