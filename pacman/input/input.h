#ifndef PACMAN_INPUT_H
#define PACMAN_INPUT_H

#include <SDL2/SDL.h>

class Input{
  private:
    SDL_Event event;
  
  public:
    Input();
    bool update();
    bool isExit() const;
    bool isWindowEvent() const;
    uint8_t getWindowEvent() const;
    SDL_Keycode getButtonPressed() const;
    bool isButtonPressed() const;
};

#endif