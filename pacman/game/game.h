#ifndef PACMAN_GAME_H
#define PACMAN_GAME_H

#include <string>
#include <pacman/graphics/graphics.h>
#include <pacman/input/input.h>
#include <pacman/screen/screen.h>
class Screen;

extern const uint8_t BLOCK;
extern const uint8_t FPS;

class Game {
  private:
    bool isRunning;
    Graphics *graphics = nullptr; 
    Input  *input = nullptr;
    Sprites *sprites = nullptr;
    Screen *screen = nullptr;
    bool loadSprites();
    bool reloadSprites();
    
  public:
    Game();
    ~Game();
    Graphics *getGraphics() const;
    Input *getInput() const;
    Sprites *getSprites() const;
    void run();
    void stop();
    void draw(const SDL_Rect *sprite, const SDL_Rect *dst);
    void drawText(const std::string text, const int x, const int y);
    void startGame();
    void openScreen(Screen *scr);
};

#endif