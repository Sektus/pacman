#include <pacman/game/game.h>
#include <pacman/screen/menu.h>
#include <pacman/screen/maze.h>
#include <iostream>
#include <ctype.h>

const uint8_t BLOCK = 12;
const uint8_t FPS = 24;

Game::Game(){
  isRunning = true;
  graphics = new Graphics();
  screen = new Menu(this);
  if (!graphics->init(screen->getWidth(), screen->getHeight()) || !loadSprites()){
    stop();
  }
  input = new Input();
  screen->start();
}

Game::~Game() {
  delete graphics;
  delete sprites;
  delete input;
  delete screen;
  SDL_Quit();
}

Graphics *Game::getGraphics() const{
  return graphics;
}

Input *Game::getInput() const{
  return input;
}

Sprites *Game::getSprites() const{
  return sprites;
}

bool Game::loadSprites(){
  return graphics->loadSprites(sprites, "pacman/res/sprite.bmp");
}

bool Game::reloadSprites(){
  delete sprites;
  return loadSprites();
}

void Game::run(){
  long curr = SDL_GetTicks();
  long prev = curr;
  while (isRunning){
    while (input->update()){
      if (input->isExit()){
          stop();
          break;
      } else if (input->isWindowEvent()){
        if (input->getWindowEvent() == SDL_WINDOWEVENT_SIZE_CHANGED){
          std::cout << "Window resized" << std::endl;
          graphics->rendererClear();
          if (!reloadSprites()){
            stop();
          }
          screen->start();
        }
      } else if (input->isButtonPressed()) {
        screen->handleEvent(input->getButtonPressed());
      }
    }
    if (curr - prev > 1000 / FPS){
      screen->update();
      graphics->render();
      prev = curr;
    }
    curr = SDL_GetTicks();
  }
}

void Game::stop(){
  isRunning = false;
}

void Game::draw(const SDL_Rect *sprite,const SDL_Rect *dst){
  graphics->draw(sprites, sprite, dst);
}

void Game::drawText(const std::string text, const int x, const int y){
  SDL_Rect sprite = {0, 2 * BLOCK, BLOCK, BLOCK};
  SDL_Rect dst = {x, y, BLOCK, BLOCK};
  for (char i: text) {
    if (isalpha(i)){ 
      sprite.x = (tolower(i) - 96) * BLOCK;
      sprite.y = 2 * BLOCK;
    } else if (isdigit(i)) {
      sprite.x = (i - 32) * BLOCK;
      sprite.y = BLOCK;
    } else if (i == '-'){
      sprite.x = 27 * BLOCK;
      sprite.y = BLOCK;
    } else {
      sprite.x = 29 * BLOCK;
      sprite.y = 0;
    }
    graphics->draw(sprites, &sprite, &dst);
    dst.x += BLOCK;
  }
}

void Game::startGame(){
  openScreen(new Maze(this));
}

void Game::openScreen(Screen *scr){
  Screen *tmp = screen;
  screen = scr;
  if (!graphics->resize(screen->getWidth(), screen->getHeight())){
    stop();
  }
  delete tmp;
}