#ifndef PACMAN_MAZE_H
#define PACMAN_MAZE_H

#include <pacman/screen/screen.h>
#include <pacman/actor/user.h>
class User;

#include <pacman/actor/ghost.h>
class Ghost;

enum State{
  PAUSE=0,
  PLAYING=1,
  EATEN_PAUSE=2,
  DYING=3,
  COUNTDOWN=4,
  END=5
};

class Maze: public Screen{
  public:
    static const uint8_t map_height = 36;
    static const uint8_t map_width = 28;
    Maze(Game *context);
    ~Maze();
    void destroy();
    void resetMap();
    void draw(SDL_Rect *src, SDL_Rect *dst);
    void start();
    void update();
    void handleEvent(const SDL_Keycode e);
    bool isWall(const int x, const int y);
    bool isFloor(const int x, const int y);
    long getTicks() const;
    Tile getUserTile() const;
    Direction getUserDirection() const;

  private:
    char map[map_height][map_width + 1];
    Graphics *graphics = nullptr;
    User *user = nullptr;
    Ghost *ghosts[4];
    State state = COUNTDOWN; 
    State savedState;
    long ticks = 0;
    long timer = 0;
    uint8_t level = 0;
    uint32_t score = 0;
    int8_t lives = 3;
    uint8_t eaten = 0;
    uint8_t eatenGhosts = 0;
    void setState(State s);
    void drawMap();
    void drawBiscuit(const SDL_Rect *dst);
    void drawPill(const SDL_Rect *dst);
    void drawWall(const int x, int y,const SDL_Rect *dst);
    void drawScore();
    void drawLives();
    void drawLevel();
    void redraw(const Point &pos);
    void redrawBlock(const int x, const int y);
    void dialog(const std::string text);
    void clearDialog();
    void animate();
    void mainLoop();
    void countdown(uint8_t sec);
    void loseLife();
    void dying();
    void pause();
    void gameEnded();
    void newGame();
    void startLevel();
    void nextLevel();
    void checkCollision();
    void tileEaten(Tile t);
    void eatenPill();
    int16_t getUserSpeed();
    int16_t getGhostSpeed();
};

#endif