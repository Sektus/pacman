#ifndef PACMAN_SCREEN_H
#define PACMAN_SCREEN_H

#include <pacman/game/game.h>
class Game;

class Screen{
  protected:
    Game* context = nullptr;
    int width = 0;
    int height = 0;
  
  public: 
    Screen(Game* context);
    ~Screen();
    int getWidth() const;
    int getHeight() const;
    virtual void start();
    virtual void update();
    virtual void destroy();
    virtual void handleEvent(SDL_Keycode e);
};

#endif