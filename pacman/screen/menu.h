#ifndef PACMAN_MENU_H
#define PACMAN_MENU_H

#include <pacman/screen/screen.h>

class Menu: public Screen {
  enum Options {
    START = 0,
    EXIT = 1
  };
  private:
    static const uint8_t OPTIONS = 2;
    Options options[OPTIONS] = {Options::START, Options::EXIT};
    int8_t pos = 0;
    SDL_Rect sprite = {45, 84, 12, 12};
    bool changed = true;

  public:
    Menu(Game *context);
    void drawOption(Options option);
    void drawPosition();
    void nextOption();
    void prevOption();
    void onMenuOptionSelected(Options option);
    virtual void start();
    virtual void update();
    virtual void destroy();
    virtual void handleEvent(SDL_Keycode e);

};

#endif