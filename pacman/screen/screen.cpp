#include <pacman/screen/screen.h>

Screen::~Screen(){
  destroy();
} 

Screen::Screen(Game *context) {
  this->context = context;
}

int Screen::getWidth() const{
  return width;
}

int Screen::getHeight() const {
  return height;
}

void Screen::start() {}
void Screen::update() {}
void Screen::destroy() {}
void Screen::handleEvent(SDL_Keycode e){}