#include <pacman/screen/maze.h>
#include <pacman/actor/user.h>
#include <pacman/actor/ghosts.h>
#include <pacman/screen/menu.h>

Maze::Maze(Game *context): Screen(context) {
  user = new User(this);
  width = map_width * BLOCK;
  height = map_height * BLOCK;
  graphics = context->getGraphics();
  ghosts[0] = new Clyde(this);
  ghosts[1] = new Blinky(this);
  ghosts[2] = new Pinky(this);
  ghosts[3] = new Inky(this);
  //resetMap();
}

void Maze::destroy() {
  delete ghosts[0];
  delete ghosts[1];
  delete ghosts[2];
  delete ghosts[3];
  delete user;
}

void Maze::resetMap() {
  static char _map[map_height][map_width + 1] = 
   {"                            ",
    "                            ",
    "                            ",
    "R[[[[[[[[[[[[lk[[[[[[[[[[[[Q",
    "T............JI............S",
    "T.H__G.H___G.JI.H___G.H__G.S",
    "T0J  I.J   I.JI.J   I.J  I0S",
    "T.LEEK.LEEEK.LK.LEEEK.LEEK.S",
    "T..........................S",
    "T.H__G.HG.H______G.HG.H__G.S",
    "T.LEEK.JI.LEEdcEEK.JI.LEEK.S",
    "T......JI....JI....JI......S",
    "V]]]]G.Je__G JI H__fI.H]]]]U",
    "     T.JcEEK LK LEEdI.S     ",
    "     T.JI          JI.S     ",
    "     T.JI N]bmma]M JI.S     ",
    "[[[[[K.LK S      T LK.L[[[[[",
    "      .   S      T   .      ",
    "]]]]]G.HG S      T HG.H]]]]]",
    "     T.JI P[[[[[[O JI.S     ",
    "     T.JI          JI.S     ",
    "     T.JI H______G JI.S     ",
    "R[[[[K.LK LEEdcEEK LK.L[[[[Q",
    "T............JI............S",
    "T.H__G.H___G.JI.H___G.H__G.S",
    "T.LEdI.LEEEK.LK.LEEEK.JcEK.S",
    "T0..JI.......  .......JI..0S",
    "X_G.JI.HG.H______G.HG.JI.H_W",
    "ZEK.LK.JI.LEEdcEEK.JI.LK.LEY",
    "T......JI....JI....JI......S",
    "T.H____fe__G.JI.H__fe____G.S",
    "T.LEEEEEEEEK.LK.LEEEEEEEEK.S",
    "T..........................S",
    "V]]]]]]]]]]]]]]]]]]]]]]]]]]U",
    "                            ",
    "                            "};

    int i, j;
    for (i = 0; i < map_height; ++i){
      for (j = 0; j < map_width; ++j){
        map[i][j] = _map[i][j];
      }
    }
}

void Maze::draw(SDL_Rect *src, SDL_Rect *dst){
  context->draw(src, dst);
}

void Maze::start(){
  newGame();
}

void Maze::update(){
  if (state != PAUSE){
    ++ticks;
  } else {
    return;
  }

  if (state == PLAYING){
    mainLoop();
  } else if (state == EATEN_PAUSE && ticks - timer > FPS){
    setState(PLAYING);
    Point pos = user->getPosition();
    pos.y -= BLOCK * 2;
    redraw(pos);
  } else if (state == COUNTDOWN){
    countdown(5);
  } else if (state == DYING){
    dying();
  } else if (state == END && std::floor((ticks - timer) / FPS) > 3){
    gameEnded();
  }
}

void Maze::handleEvent(const SDL_Keycode e){
  switch (e){
    case SDLK_p:
      pause();
      break;
    default:
      user->handleKey(e);
      break;
  }
}

bool Maze::isFloor(const int x, const int y){
  char type = map[y][x];
  if (y == 17 && (x == -1 || x >= map_width)){
    return true;
  }
  if (type == '.' || type == '0' || type == ' '){
    return true;
  }
  return false;
}

bool Maze::isWall(const int x, const int y){
  return !isFloor(x, y);
}

long Maze::getTicks() const {
  return ticks;
}

Tile Maze::getUserTile() const{
  return user->getTile();
}

Direction Maze::getUserDirection() const {
  return user->getDirection();
}

void Maze::setState(State s){
  state = s;
}

void Maze::drawMap(){
  int x, y;
  SDL_Rect dst = {0, 0, BLOCK, BLOCK};
  for (y = 0; y < map_height; ++y){
    for (x = 0; x < map_width; ++x){
      dst.x = x * BLOCK;
      dst.y = y * BLOCK;
      if (map[y][x] == '.'){
        drawBiscuit(&dst);
      } else if (map[y][x] =='0'){
        drawPill(&dst);
      } else if (map[y][x] != ' '){
        drawWall(x, y, &dst);
      } 
    }
  }
}

void Maze::drawBiscuit(const SDL_Rect *dst){
  SDL_Rect sprite = {192, 0, BLOCK, BLOCK};
  context->draw(&sprite, dst);
}

void Maze::drawPill(const SDL_Rect *dst){
  SDL_Rect sprite = {240, 0, BLOCK, BLOCK};
  context->draw(&sprite, dst);
}

void Maze::drawWall(const int x, const int y, const SDL_Rect *dst){
  int n = map[y][x] - 'A';
  SDL_Rect sprite = {0, 0, BLOCK, BLOCK};
  sprite.x = 192 + BLOCK * (n % 16);
  sprite.y = 36 + BLOCK * (static_cast<int>(n / 16));
  context->draw(&sprite, dst);
}

void Maze::drawScore(){
  std::string text = std::to_string(score);
  SDL_Rect dst = {5 * BLOCK, BLOCK, text.length() * BLOCK, BLOCK};
  graphics->fillRect(&dst);
  context->drawText(text, 5 * BLOCK, BLOCK);
}

void Maze::drawLevel(){
  std::string text = std::to_string(level) + "up";
  SDL_Rect dst = {18 * BLOCK, BLOCK, text.length() * BLOCK, BLOCK};
  graphics->fillRect(&dst);
  context->drawText(text, 18 * BLOCK, BLOCK);
}

void Maze::drawLives(){
  SDL_Rect dst = {2 * BLOCK, 34 * BLOCK, (lives + 2) * BLOCK * 2, BLOCK * 2};
  graphics->fillRect(&dst);
  dst.w = 2 * BLOCK;
  dst.h = 2 * BLOCK;
  SDL_Rect sprite = {48, 72, BLOCK * 2, BLOCK * 2};
  for (int i = 0; i < lives; ++i){
    dst.x += 2 * BLOCK;
    context->draw(&sprite, &dst);
  }
}

void Maze::redraw(const Point &pos){
  const Tile tile = Tile(pos);

  //fills previous user position with black color
  SDL_Rect box = {pos.x - BLOCK, pos.y - BLOCK, BLOCK * 2, BLOCK * 2};
  graphics->fillRect(&box);

  int x, y;
  for (x = -1; x < 2; ++x){
    for (y = -1; y < 2; ++y){
      redrawBlock(tile.x + x, tile.y + y);
    }
  }
}

void Maze::redrawBlock(const int x, const int y){
  SDL_Rect dst = {x * BLOCK, y * BLOCK, BLOCK, BLOCK};
  if (map[y][x] == '.'){
    drawBiscuit(&dst);
  } else if (map[y][x] =='0'){
    drawPill(&dst);
  } else if (map[y][x] != ' '){
    drawWall(x, y, &dst);
  } else if (map[y][x] == ' '){
    graphics->fillRect(&dst);
  }
}

void Maze::dialog(const std::string text){
  clearDialog();
  context->drawText(text, 14 * BLOCK - BLOCK * text.length() / 2, 20 * BLOCK);
}

void Maze::clearDialog(){
  SDL_Rect dst = {9 * BLOCK, 20 * BLOCK, 10 * BLOCK, BLOCK};
  graphics->fillRect(&dst);
}

void Maze::animate(){
  int x, y;
  int dt = ticks % 47 / 8;
  if (dt == 0 || dt == 5){
    dt = 0;
  } else if (dt == 1 || dt == 4){
    dt = 1;
  } else {
    dt = 2;
  }
  SDL_Rect src = { 240 - dt * BLOCK * 2, 0, BLOCK, BLOCK };
  SDL_Rect dst = {0, 0, BLOCK, BLOCK};
  for (x = 1; x <= 26; x += 25 ){
    dst.x = x * BLOCK;
    for (y = 6; y < 27; y += 20){
      dst.y = y * BLOCK;
      if (map[y][x] == '0'){
        graphics->fillRect(&dst);
        draw(&src, &dst);
      }
    }
  }
}

void Maze::mainLoop(){
  animate();
  Point prev = user->move();
  redraw(prev);

  for (int i = 0; i < 4; ++i){
    prev = ghosts[i]->move();
    redraw(prev);
  }
  checkCollision();
  
  if (state != DYING){
    tileEaten(user->getTile());
    for (int i = 0; i < 4; ++i){
      ghosts[i]->draw();
    }
  } 
  user->draw();
}

void Maze::countdown(uint8_t sec){
  long diff = sec + static_cast<int>(std::floor((timer - ticks) / FPS));
  if (diff == 0){
    setState(PLAYING);
    clearDialog();
  } else {
    dialog("Start in " + std::to_string(diff));
  }
}

void Maze::loseLife(){
  if (--lives >= 0){
    drawLives();
    startLevel();
  } else {
    dialog("your score " + std::to_string(score));
    timer = ticks;
    setState(END);
  }
}

void Maze::dying(){
  int diff = ticks - timer;
  if (diff > FPS * 3){
    loseLife();
  } else {
    Point pos = user->getPosition();
    redraw(pos);
    user->drawDead(diff / 6);
  }
}

void Maze::pause(){
  if (state != PAUSE ){
    dialog("pause");
    savedState = state;
    setState(PAUSE);
  } else {
    setState(savedState);
    clearDialog();
  }
}

void Maze::gameEnded() {
  context->openScreen(new Menu(context));
}

void Maze::newGame(){
  level = 0;
  lives = 3;
  score = 0;

  nextLevel();
  drawScore();
  drawLives();
}

void Maze::startLevel(){
  drawMap();
  user->reset();
  user->draw();
  for (int i = 0; i < 4; ++i){
    ghosts[i]->reset();
    ghosts[i]->draw();
  }
  timer = ticks;
  setState(COUNTDOWN);
}

void Maze::nextLevel(){
  eaten = 0;
  ++level;

  //draw level text
  resetMap();
  drawLevel();

  user->setSpeed(getUserSpeed());
  for (uint8_t i = 0; i < 4; ++i){
    ghosts[i]->setSpeed(getGhostSpeed());
  }

  startLevel();
}

void Maze::checkCollision(){
  for (int i = 0; i < 4; ++i){
    if (user->collided(ghosts[i])){
      if (ghosts[i]->isVunerable()){
        ghosts[i]->eat();

        Point pos = ghosts[i]->getPosition();
        SDL_Rect dst = {pos.x - BLOCK, pos.y - BLOCK * 3, 2 * BLOCK, 2 * BLOCK};
        SDL_Rect src = {192 + eatenGhosts * 24, 144, 2 * BLOCK, 2 * BLOCK};
        context->draw(&src, &dst);

        score += std::pow(2, ++eatenGhosts) * 100;
        drawScore();

        timer = ticks;
        setState(EATEN_PAUSE);
      } else if (ghosts[i]->isDangerous()){
        timer = ticks;
        setState(DYING);
      }
    }
  }
}

void Maze::tileEaten(Tile t){
  char type = map[t.y][t.x];
  if (type == '.' || type == '0'){
    map[t.y][t.x] = ' ';

    ++eaten;
    if (type == '.'){
      score += 10;
    } else {
      score += 50;
      eatenPill();
    }
    drawScore();

    if (eaten == 244){
      nextLevel();
    }
  }
}

void Maze::eatenPill(){
  eatenGhosts = 0;
  for (int i = 0; i < 4; ++i){
    ghosts[i]->makeEatable();
  }
}

int16_t Maze::getGhostSpeed(){
  int16_t speed = BLOCK / 4;
  if (level == 1){
    return 75 * speed;
  } else if (level <= 4) {
    return 85 * speed;
  } else if (level <= 20){
    return 95 * speed;
  }
  return 100 * speed;
}

int16_t Maze::getUserSpeed(){
  int16_t speed = BLOCK / 4;
  if (level == 1){
    return 80 * speed;
  } else if (level <= 4) {
    return 90 * speed;
  } else if (level <= 20){
    return 100 * speed;
  }
  return 90 * speed;
}