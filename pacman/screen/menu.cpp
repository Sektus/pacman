#include <pacman/screen/menu.h>
#include <iostream>

Menu::Menu(Game *context): Screen(context){
  width = 168;
  height = 216;
}

void Menu::start(){
  context->getGraphics()->rendererClear();
  for (auto option: options){
    drawOption(option);
  }
  drawPosition();
}

void Menu::drawOption(Options option){
  switch (option){
    case Options::START:
      context->drawText("start", 60, 84);
      break;
    case Options::EXIT:
      context->drawText("exit", 60, 99);
      break;
  }
}

void Menu::update(){
  drawPosition();
}

void Menu::drawPosition(){
  if (changed){
    Graphics* graphics = context->getGraphics();
    changed = false;
    graphics->fillRect(&sprite);
    sprite.y = 84 + pos * 15;
    context->drawText("-", 45, 84 + pos * 15);
  }
}

void Menu::nextOption(){
  changed = true;
  if (++pos >= OPTIONS){
    pos = 0;
  }
}

void Menu::prevOption(){
  changed = true;
  if (--pos < 0){
    pos = OPTIONS - 1;
  }
}

void Menu::destroy(){}

void Menu::handleEvent(SDL_Keycode e){
  switch (e){
    case SDLK_UP:
      prevOption();
      break;
    case SDLK_DOWN:
      nextOption();
      break;
    case SDLK_RETURN:
      onMenuOptionSelected(options[pos]);
      break;
  }
}

void Menu::onMenuOptionSelected(Options option){
  switch (option){
    case Options::START:
      context->startGame();
      break;
    case Options::EXIT:
      context->stop();
      break;
  }
}