#include <pacman/graphics/graphics.h>

Graphics::Graphics(){}
Graphics::~Graphics(){
  destroy();
}

bool Graphics::createWindow(){
  //Set texture filtering to linear
  if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ){
    std::cout << "Warning: Linear texture filtering not enabled!" << std::endl;
  }

  window = SDL_CreateWindow("Pacman", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
  if (window == nullptr) {
    std::cout << "Window could not be created! SDL Error:" << SDL_GetError() << std::endl;
    return false;
  }

  return true;
}

bool Graphics::createRenderer() {
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  //SDL_SetHint(SDL_HINT_RENDER_DRIVER, "opengl");
  if (renderer == nullptr){
    std::cout << "Renderer could not be created! SDL Error:" << SDL_GetError() << std::endl;
    return false;
  } 
  setColor(0x00, 0x00, 0x00, 0xFF);
  return true;
}

bool Graphics::recreateRenderer(){
  SDL_DestroyRenderer(renderer);
  return createRenderer();
}

bool Graphics::init(const uint16_t w, const uint16_t h){
  if (SDL_Init( SDL_INIT_VIDEO) < 0) {
    std::cout << "SDL could not initialie! SDL Error:" << SDL_GetError() << std::endl;
    return false;
  } else {
    width = w;
    height = h;
    if (createWindow() && createRenderer()){
      return true;
    } else {
      return false;
    }
  }
}

void Graphics::destroy(){
  SDL_DestroyRenderer(renderer);
  renderer = nullptr;
  SDL_DestroyWindow(window);
  window = nullptr;
}

/* Resize window and recreate renderer */
bool Graphics::resize(const uint16_t w, const uint16_t h){
  if (width != w || height != h){
    width = w;
    height = h;
    SDL_SetWindowSize(window, width, height);
    SDL_SetWindowPosition(window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
    if (recreateRenderer()){
      //rendererClear();
    } else {
      return false;
    }
  }
  return true;
}

/* Load sprites from .bmp file */
bool Graphics::loadSprites(Sprites *& sprites, const std::string &fn){
  SDL_Surface *surface = SDL_LoadBMP(fn.c_str());
  if (surface == nullptr) {
     std::cout << "Surface could not be loaded from file " << fn << "! SDL Error:" << SDL_GetError() << std::endl;
     return false;
  }

  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
  if (texture == nullptr) {
    std::cout << "Texture could not be loaded from file " << fn << "! SDL Error:" << SDL_GetError() << std::endl;
     return false;
  }
  sprites = new Sprites(texture, surface->w, surface->h);
  SDL_FreeSurface(surface);
  return true;
}

void Graphics::fillRect(const SDL_Rect *rect){
  SDL_RenderFillRect(renderer, rect);
}

void Graphics::draw(const Sprites *sprites, const SDL_Rect *src, const SDL_Rect *dst){
  SDL_RenderCopy(renderer, sprites->getTexture(), src, dst);
}

void Graphics::render() {
  SDL_RenderPresent(renderer);
}

void Graphics::rendererClear() {
  setColor(0x00, 0x00, 0x00, 0xFF);
  SDL_RenderClear(renderer);
}

void Graphics::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a){
  SDL_SetRenderDrawColor(renderer, r, g, b, a);
}