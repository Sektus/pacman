#ifndef PACMAN_GRAPHICS_H
#define PACMAN_GRAPHICS_H

#include <SDL2/SDL.h>
#include <iostream>
#include <pacman/graphics/sprites.h>
class Sprites;

class Graphics{
  private:
    SDL_Window *window = nullptr;
    SDL_Renderer *renderer = nullptr;
    SDL_Texture *texture = nullptr;
    uint16_t width = 0;
    uint16_t height = 0;
    bool createWindow();
    bool createRenderer();
    bool recreateRenderer();

  public:
    Graphics();
    ~Graphics();
    bool init(const uint16_t w,const uint16_t h);
    void destroy();
    bool resize(const uint16_t w, const uint16_t h);
    bool loadSprites(Sprites *& sprites, const std::string &fn);
    void fillRect(const SDL_Rect *rect);
    void draw(const Sprites *sprites, const SDL_Rect *src, const SDL_Rect *dst);
    void render();
    void rendererClear();
    void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xFF);
};



#endif