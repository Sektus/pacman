#ifndef PACMAN_SPRITES_H
#define PACMAN_SPRITES_H

#include <SDL2/SDL.h>
#include <string>

class Sprites{
  private: 
    SDL_Texture* texture = nullptr;
    uint16_t width;
    uint16_t height;
  
  public:
    Sprites(SDL_Texture *t, const uint16_t w, const uint16_t h);
    ~Sprites();
    uint16_t getWidth() const;
    uint16_t getHeight() const;
    SDL_Texture *getTexture() const;
    SDL_Rect getSprite(const uint16_t x, const uint16_t y, const uint16_t w = 24, const uint16_t h = 24);
    void destroy();
};

#endif