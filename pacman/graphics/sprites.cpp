#include <pacman/graphics/sprites.h>

Sprites::Sprites(SDL_Texture *t, const uint16_t w, const uint16_t h): texture(t), width(w), height(h) {}

Sprites::~Sprites() {
  destroy();
}

uint16_t Sprites::getHeight() const {
  return height;
}

uint16_t Sprites::getWidth() const {
  return width;
}

SDL_Texture *Sprites::getTexture() const {
  return texture;
}

SDL_Rect Sprites::getSprite(const uint16_t x, const uint16_t y, const uint16_t w, const uint16_t h) {
  SDL_Rect clip = {x * w, y * h, w, h };
  return clip;
}

void Sprites::destroy() {
  SDL_DestroyTexture(texture);
}