TARGET := pacman.out
RM := rm

#directories
OBJDIR := ./.obj
SRCDIR := ./pacman
DEPDIR := $(OBJDIR)/.deps

#files
SRCS := $(shell find $(SRCDIR) -type f -name "*.cpp")
HEADS := $(shell find $(SRCDIR) -type f -name "*.h")
OBJS := $(SRCS:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

#cc flags
CXX := g++

INCLUDES := -I.
LDFLAGS := -lm -lSDL2 -lSDL2_ttf
CPPFLAGS = -c $< -O3 -w $(INCLUDES)
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.d
WARNINGS := -Wall

COMPILE.cpp = $(CXX) $(WARNINGS) $(DEPFLAGS) $(CPPFLAGS)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(DEPDIR)/%.d | $(DEPDIR)
	@mkdir -p $(@D)
	$(COMPILE.cpp) -o $@ 

.PHONY: clean all

clean:
	$(RM) -rf $(OBJDIR) $(TARGET)

$(DEPDIR): ; @mkdir -p $@

DEPFILES = $(SRCS:$(SRCDIR)/%.cpp=$(DEPDIR)/%.d)
$(DEPFILES):
	@mkdir -p $(@D)

include $(wildcard $(DEPFILES))
